package com.thisisglobal.directoryservices.grtc;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONArray;

import com.thisisglobal.directoryservices.grtc.CallClient.ClientState;

/**
 * Server for managing calls.
 * @author Marc Steele.
 */

public class CallSocketServer extends WebSocketServer {
	
	private Map<String, CallClient> clients = new HashMap<String, CallClient>();
	private LDAPClient ldapClient = new LDAPClient();
	private static Logger logger = Logger.getLogger(CallSocketServer.class.getName());
	
	/**
	 * Creates a new instance of the server.
	 * @param port The port we'll be creating it on.
	 * @param draft The draft. Apparently it's needed by the library.
	 */
	
	public CallSocketServer(int port, Draft draft) {
		super(new InetSocketAddress(port), Collections.singletonList(draft));
	}
	
	/**
	 * Creates a new instance of the server.
	 * @param address The interface and port we'll be creating it on.
	 * @param draft The draft. Apparently it's needed by the library.
	 */
	
	public CallSocketServer(InetSocketAddress address, Draft draft) {
		super(address, Collections.singletonList(draft));
	}

	@Override
	public void onClose(WebSocket webSocket, int code, String reason, boolean remote) {
		this.logOut(webSocket);
	}

	@Override
	public void onError(WebSocket arg0, Exception arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessage(WebSocket webSocket, String message) {
		
		// Sanity check
		
		if (message == null || message.isEmpty()) {
			logger.log(Level.WARNING, String.format("Got an empty message from %s.", webSocket.getRemoteSocketAddress()));
			return;
		}
		
		logger.log(Level.INFO, String.format("Message from %s: %s", webSocket.getRemoteSocketAddress(), message));
		
		// Work out what action we need to take
		
		String[] splitMessage = message.split(" ", 2);
		String messageType = splitMessage[0];
		
		if (messageType.equalsIgnoreCase("AUTHENTICATE")) {
			this.authenticateUser(webSocket, splitMessage);
		} else if (messageType.equalsIgnoreCase("LOGOUT")) {
			this.logOut(webSocket);
		} else if (messageType.equalsIgnoreCase("USERS")) {
			webSocket.send(this.getUserListMessage());
		} else if (messageType.equalsIgnoreCase("DIAL")) {
			this.dial(webSocket, splitMessage);
		} else if (messageType.equalsIgnoreCase("ACCEPT")) {
			this.accept(webSocket, splitMessage);
		} else if (messageType.equalsIgnoreCase("HANGUP")) {
			this.hangup(webSocket, splitMessage);
		}

	}
	
	/**
	 * Hangs up a call.
	 * @param webSocket The web socket we're on.
	 * @param splitMessage The message from the user.
	 */
	
	private void hangup(WebSocket webSocket, String[] splitMessage) {
		
		// Find out who we are
		
		String fromUsername = this.getUsernameFromSocket(webSocket);
		CallClient fromClient = this.clients.get(fromUsername);
		
		if (fromClient == null) {
			logger.log(Level.WARNING, "Asked to hang up but we don't know the user.");
			webSocket.send("FAIL User not authenticated.");
			return;
		}
		
		// Find out who we're hanging up on
		
		if (splitMessage.length < 2) {
			logger.log(Level.WARNING, String.format("Asked by %s to hang up but never told who to hang up on!", fromUsername));
			webSocket.send("FAIL You need to tell us who you're hanging up on.");
			return;
		}
		
		CallClient toClient = this.clients.get(splitMessage[1]);
		if (toClient == null) {
			logger.log(Level.WARNING, String.format("Asked by %s to hang up on %s but we can't find them.", fromUsername, splitMessage[1]));
			webSocket.send("FAIL Cannot find the user you want to hang up on.");
			return;
		}
		
		// Perform the hang up
		
		fromClient.setState(ClientState.Idle);
		toClient.getWebSocket().send("HANGUP");
		
	}
	
	/**
	 * Accepts a call and passes the parameters onto the other user.
	 * @param webSocket The socket we got the accept from.
	 * @param splitMessage The message from the user.
	 */
	
	private void accept(WebSocket webSocket, String[] splitMessage) {
		
		// Make sure it's going to a destination
		
		if (splitMessage.length < 2) {
			logger.log(Level.WARNING, "Asked to accpet a call but not told who the call's with.");
			webSocket.send("FAIL Missing information.");
			return;
		}
		
		String[] splitArguments = splitMessage[1].split(" ", 2);
		
		if (splitArguments.length < 2) {
			logger.log(Level.WARNING, String.format("Asked to accept a call from %s but not given the technical parameters.", splitArguments[0]));
			webSocket.send("FAIL Missing technical parameters.");
			return;
		}
		
		String acceptTo = splitArguments[0];
		String techParams = splitArguments[1];
		
		// Figure out who we are
		
		String acceptFrom = this.getUsernameFromSocket(webSocket);
		CallClient fromClient = this.clients.get(acceptFrom);
		
		if (fromClient == null) {
			logger.log(Level.WARNING, String.format("Could not accept call from %s as we don't know who sent it.", acceptTo));
			webSocket.send("FAIL Not authenticated.");
			return;
		}
		
		// Figure out who we're sending to
		
		CallClient toClient = this.clients.get(acceptTo);
		if (toClient == null) {
			logger.log(Level.WARNING, String.format("Could not accept the call from %s to %s as we couldn't find the user we're supposed to send the accept to.", acceptFrom, acceptTo));
			webSocket.send("FAIL No user of that name found.");
			return;
		}
		
		// Change the state
		
		fromClient.setState(ClientState.InCall);
		toClient.getWebSocket().send(String.format("ACCEPT %s %s", fromClient.getUsername(), techParams));
		logger.log(Level.INFO, String.format("%s is now in a call.", fromClient.getUsername()));
		
	}
	
	/**
	 * Attempts to dial out to a user.
	 * @param webSocket The web socket we're connecting from.
	 * @param splitMessage The message we got from the user.
	 */
	
	private void dial(WebSocket webSocket, String[] splitMessage) {
		
		// Make sure we've got a destination to dial
		
		if (splitMessage.length < 2) {
			logger.log(Level.WARNING, "Asked to dial but not given a user to dial.");
			webSocket.send("FAIL No destination given.");
			return;
		}
		
		// Check our destination
		
		String remoteUser = splitMessage[1];
		CallClient remoteClient = this.clients.get(remoteUser);
		
		if (remoteClient == null) {
			logger.log(Level.WARNING, String.format("Ask to dial the user %s but it turns out they're offline.", remoteUser));
			webSocket.send("FAIL Remote user offline.");
			return;
		}
		
		if (remoteClient.getState() != ClientState.Idle) {
			logger.log(Level.WARNING, String.format("Cannot dial %s as they're currently busy.", remoteUser));
			webSocket.send("FAIL Engaged.");
			return;
		}
		
		// Change the states
		
		String currentUsername = this.getUsernameFromSocket(webSocket);
		CallClient localClient = this.clients.get(currentUsername);
		
		localClient.setState(ClientState.Calling);
		remoteClient.setState(ClientState.Ringing);
		
		// Ok, let's ring them
		
		localClient.getWebSocket().send(String.format("RINGING %s", remoteUser));
		remoteClient.getWebSocket().send(String.format("RING %s", currentUsername));
		
	}
	
	/**
	 * Logs a user out from the server.
	 * @param webSocket The connection the user is on.
	 */
	
	private void logOut(WebSocket webSocket) {
		
		// Log the user out (if we found them)
		
		String username = this.getUsernameFromSocket(webSocket);
		
		if (username != null && !username.isEmpty()) {
			
			this.clients.remove(username);
			webSocket.send("GOODBYE");
			
			if (!webSocket.isClosing()) {
				webSocket.close(0);
			}
		}
		
		// Better tell everyone
		
		this.sentToAll(this.getUserListMessage());
		
	}
	
	/**
	 * Attempts to authenticate the user.
	 * @param webSocket The web socket the user is on.
	 * @param splitMessage The message we got from the user.
	 */

	private void authenticateUser(WebSocket webSocket, String[] splitMessage) {
		
		// We need to authenticate the user
		// Pull out the username and password
		
		if (splitMessage.length < 2) {
			webSocket.send("AUTH_FAIL No username and password supplied.");
			logger.log(Level.INFO, "Failed to authenticate. No username and password supplied.");
			return;
		}
		
		String[] usernamePassword = splitMessage[1].split(" ", 2);
		if (usernamePassword.length < 2) {
			webSocket.send("AUTH_FAIL No username and password supplied.");
			logger.log(Level.INFO, "Failed to authenticate. No username and password supplied.");
			return;
		}
		
		// Now check the credentials
		
		String username = usernamePassword[0];
		String password = usernamePassword[1];
		
		String userDN = this.ldapClient.getDN(username);
		if (this.ldapClient.checkCredentials(userDN, password)) {
			
			// All OK. Accept the user
			
			CallClient client = new CallClient();
			
			client.setUsername(username);
			client.setState(ClientState.Idle);
			client.setWebSocket(webSocket);
			
			this.clients.put(username, client);
			
			// Let them know
			
			webSocket.send("AUTH_OK");
			logger.log(Level.INFO, String.format("Successfully authenticated %s.", username));
			
			// And tell everyone else
			
			this.refreshAllUserLists();
			
		} else {
			
			// Fail - throw it back
			
			webSocket.send("AUTH_FAIL Invalid username and/or password.");
			logger.log(Level.INFO, "Failed to authenticate. Invalid username and/or password.");
			
			// Make sure we remove any previous success
			
			if (this.clients.containsKey(username)) {
				this.clients.remove(username);
			}
			
		}
	}
	
	/**
	 * Refreshes everyone's user lists.
	 */
	
	private void refreshAllUserLists() {
		this.sentToAll(this.getUserListMessage());
	}
	
	/**
	 * Sends a message to all users.
	 * @param message The message to send.
	 */
	
	private void sentToAll(String message) {
		
		// Sanity check
		
		if (message == null || message.isEmpty()) {
			logger.log(Level.WARNING, "Asked to send an empty message to all users.");
			return;
		} else {
			logger.log(Level.INFO, String.format("Sending message to all users: %s", message));
		}
		
		// Actually send the message
		
		for (CallClient currentClient:this.clients.values()) {
			currentClient.getWebSocket().send(message);
		}
		
	}
	
	/**
	 * Generates the user list message.
	 * @return The user list message.
	 */
	
	private String getUserListMessage() {
		
		JSONArray usernameArray = new JSONArray();
		for (String currentUsername:this.clients.keySet()) {
			usernameArray.add(currentUsername);
		}
		
		return String.format("USERS %s", usernameArray.toJSONString());	
		
	}

	@Override
	public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
		logger.log(Level.INFO, String.format("New connection from %s.", webSocket.getRemoteSocketAddress()));
	}
	
	/**
	 * Tries to obtain a username from a web socket.
	 * @param webSocket The web socket we want to identify the user for.
	 * @return The username, if we can find it. Otherwise null.
	 */
	
	private String getUsernameFromSocket(WebSocket webSocket) {
		
		for (CallClient currentClient:this.clients.values()) {
			
			if (currentClient.getWebSocket().equals(webSocket)) {
				return currentClient.getUsername();
			}
			
		}
		
		return null;
		
	}

}
