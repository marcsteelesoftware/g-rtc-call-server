package com.thisisglobal.directoryservices.grtc;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;

public class LDAPClient {
	
	private static Logger logger = Logger.getLogger(LDAPClient.class.getName());
	
	private static final String USER_SEARCH_BASE = "OU=Global,dc=thisisglobal,dc=com";
	private static final String LDAP_SERVER = "thisisglobal.com";
	private static final int LDAP_PORT = 389;
	private static final String LDAP_USERNAME = "CN=Service CSI LDAP,OU=ServiceAccounts,OU=Admin,DC=thisisglobal,DC=com";
	private static final String LDAP_PASSWORD = "Wh0Ar3You?";
	
	/**
	 * Connects to and binds against the LDAP server.
	 * @return The connection if we were successful. Otherwise NULL.
	 */
	
	private LDAPConnection connectAndBind() {
		
		LDAPConnection conn = new LDAPConnection();
		
		try {
			
			conn.connect(LDAP_SERVER, LDAP_PORT);
			conn.bind(LDAP_USERNAME, LDAP_PASSWORD);
			
		} catch (LDAPException e) {
			logger.log(Level.SEVERE, "Ran into an LDAP error trying to connect and bind to the server.", e);
			return null;
		}
		
		logger.log(Level.INFO, String.format("Successfully connected to the LDAP server at %1$s:%2$d", LDAP_SERVER, LDAP_PORT));
		return conn;
		
	}
	
	/**
	 * Gets the full DN for a specific username.
	 * @param username The username we're interested in.
	 * @return The DN, if we can find it. Otherwise NULL.
	 */
	
	public String getDN(String username) {
		
		// Sanity checks
		
		if (username == null || username.isEmpty()) {
			logger.log(Level.WARNING, "Cannot get the DN of an empty/null username.");
			return null;
		}
		
		// Connect to the LDAP server
		
		LDAPConnection conn = this.connectAndBind();
		if (conn == null) {
			logger.log(Level.SEVERE, String.format("Cannot get the DN for a user as we failed to connect to the LDAP server at %s.", LDAP_SERVER));
			return null;
		}
		
		// Perform the search
		
		String dn = null;
		
		try {
			
			String searchFilter = String.format("(userPrincipalName=%1$s@*)", username);
			LDAPSearchResults results = conn.search(USER_SEARCH_BASE, LDAPConnection.SCOPE_SUB, searchFilter, null, false);
			
			while (results.hasMore()) {
				LDAPEntry currentEntry = results.next();
				dn = currentEntry.getDN();
			}
			
		} catch (LDAPException e) {
			logger.log(Level.WARNING, "Ran into a problem trying to find a user's DN.", e);
		}
		
		// Cleanup
		
		try {
			conn.disconnect();
		} catch (LDAPException e) {
			logger.log(Level.WARNING, "Ran into a problem disconnecting from the LDAP server.", e);
		}
		
		return dn;
		
	}
	
	/**
	 * Checks the DN and password of a user are valid.
	 * @param dn The DN we're checking the credentials for.
	 * @param password The password we're checking.
	 * @return TRUE if everything is valid. Otherwise FALSE.
	 */
	
	public boolean checkCredentials(String dn, String password) {
		
		boolean success = false;
		
		// We're going to check by binding.
		// If the process fails, we assume the user details are wrong
		
		LDAPConnection conn = new LDAPConnection();
		
		try {
			
			conn.connect(LDAP_SERVER, LDAP_PORT);
			conn.bind(dn, password);
			success = conn.isBound();
			
		} catch (LDAPException e) {
			logger.log(Level.INFO, String.format("Failed to bind with the supplied password for %s.", dn), e);
			success = false;
		}
		
		// Success. Let's disconnect.
		
		try {
			conn.disconnect();
		} catch (LDAPException e) {
			logger.log(Level.INFO, "Ran into a problem disconnecting from the LDAP server.", e);
		}
		
		return success;
		
		
	}

}
