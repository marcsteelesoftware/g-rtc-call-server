package com.thisisglobal.directoryservices.grtc;

import org.java_websocket.drafts.Draft_17;

/**
 * Launches the application.
 * @author Marc Steele
 */

public class Launcher {

	/**
	 * Launches the application.
	 * @param args The command line arguments.
	 */
	
	public static void main(String[] args) {
		CallSocketServer server = new CallSocketServer(1234, new Draft_17());
		server.start();
	}

}
