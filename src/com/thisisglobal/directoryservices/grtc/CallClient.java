package com.thisisglobal.directoryservices.grtc;

import org.java_websocket.WebSocket;

/**
 * Represents a client connected to the server.
 * @author Marc Steele
 */

public class CallClient {
	
	private String username;
	private ClientState state = ClientState.Authenticating;
	private WebSocket webSocket;
	
	public enum ClientState {
		Authenticating,
		Idle,
		Calling,
		Ringing,
		InCall
	}
	
	/**
	 * Obtains the username this client is registered as.
	 * @return The username this client is registered as.
	 */

	public String getUsername() {
		return username;
	}
	
	/**
	 * Sets the username this client is registered as.
	 * @param username The username this client is registered as.
	 */

	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Obtains the state this client is currently in.
	 * @return The state this client is currently in.
	 */

	public ClientState getState() {
		return state;
	}
	
	/**
	 * Sets the state the client is currently in.
	 * @param state The state the client is currently in.
	 */

	public void setState(ClientState state) {
		this.state = state;
	}
	
	/**
	 * Obtains the web socket we communicate with the client on.
	 * @return The web socket we communicate with the client on.
	 */

	public WebSocket getWebSocket() {
		return webSocket;
	}
	
	/**
	 * Sets the web socket we communicate with the client on.
	 * @param webSocket The web socket we communicate with the client on.
	 */

	public void setWebSocket(WebSocket webSocket) {
		this.webSocket = webSocket;
	}

}
